
# CloudTables Angular client

To get started with development for the Angular client:

```
npm install
```

The code for the library is in `projects/ct-library`. The package.json skeleton is in there as well and should be modified as required. To build the software:

```
./node_modules/.bin/ng build ct-component
```

The library of interest being built is in `dist/ct-library`.
