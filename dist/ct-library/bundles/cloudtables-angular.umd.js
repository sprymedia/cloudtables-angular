(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('@cloudtables/angular', ['exports', '@angular/core'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory((global.cloudtables = global.cloudtables || {}, global.cloudtables.angular = {}), global.ng.core));
}(this, (function (exports, i0) { 'use strict';

    function _interopNamespace(e) {
        if (e && e.__esModule) return e;
        var n = Object.create(null);
        if (e) {
            Object.keys(e).forEach(function (k) {
                if (k !== 'default') {
                    var d = Object.getOwnPropertyDescriptor(e, k);
                    Object.defineProperty(n, k, d.get ? d : {
                        enumerable: true,
                        get: function () {
                            return e[k];
                        }
                    });
                }
            });
        }
        n['default'] = e;
        return Object.freeze(n);
    }

    var i0__namespace = /*#__PURE__*/_interopNamespace(i0);

    var CloudTablesService = /** @class */ (function () {
        function CloudTablesService() {
        }
        return CloudTablesService;
    }());
    CloudTablesService.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesService, deps: [], target: i0__namespace.ɵɵFactoryTarget.Injectable });
    CloudTablesService.ɵprov = i0__namespace.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesService, providedIn: 'root' });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesService, decorators: [{
                type: i0.Injectable,
                args: [{
                        providedIn: 'root'
                    }]
            }], ctorParameters: function () { return []; } });

    var CloudTablesComponent = /** @class */ (function () {
        function CloudTablesComponent(renderer, el) {
            this.renderer = renderer;
            this.el = el;
        }
        CloudTablesComponent.prototype.ngOnInit = function () {
            var data = this.el.nativeElement.dataset;
            console.log(data);
            var script = this.renderer.createElement('script');
            if (data.src) {
                script.src = data.src;
            }
            if (data.clientid) {
                script.setAttribute('data-clientId', data.clientid);
            }
            if (data.token) {
                script.setAttribute('data-token', data.token);
            }
            if (data.apikey) {
                script.setAttribute('data-apiKey', data.apikey);
            }
            if (data.clientname) {
                script.setAttribute('data-clientName', data.clientnameS);
            }
            this.renderer.appendChild(this.el.nativeElement, script);
        };
        return CloudTablesComponent;
    }());
    CloudTablesComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesComponent, deps: [{ token: i0__namespace.Renderer2 }, { token: i0__namespace.ElementRef }], target: i0__namespace.ɵɵFactoryTarget.Component });
    CloudTablesComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.0", type: CloudTablesComponent, selector: "cloud-tables", ngImport: i0__namespace, template: '', isInline: true });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'cloud-tables',
                        template: '',
                        styles: []
                    }]
            }], ctorParameters: function () { return [{ type: i0__namespace.Renderer2 }, { type: i0__namespace.ElementRef }]; } });

    var CloudTablesModule = /** @class */ (function () {
        function CloudTablesModule() {
        }
        return CloudTablesModule;
    }());
    CloudTablesModule.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesModule, deps: [], target: i0__namespace.ɵɵFactoryTarget.NgModule });
    CloudTablesModule.ɵmod = i0__namespace.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesModule, declarations: [CloudTablesComponent], exports: [CloudTablesComponent] });
    CloudTablesModule.ɵinj = i0__namespace.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesModule, imports: [[]] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0__namespace, type: CloudTablesModule, decorators: [{
                type: i0.NgModule,
                args: [{
                        declarations: [
                            CloudTablesComponent
                        ],
                        imports: [],
                        exports: [
                            CloudTablesComponent
                        ]
                    }]
            }] });

    /*
     * Public API Surface of ct-library
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.CloudTablesComponent = CloudTablesComponent;
    exports.CloudTablesModule = CloudTablesModule;
    exports.CloudTablesService = CloudTablesService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=cloudtables-angular.umd.js.map
