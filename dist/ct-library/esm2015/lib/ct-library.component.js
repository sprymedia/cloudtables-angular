import { Component } from '@angular/core';
import * as i0 from "@angular/core";
export class CloudTablesComponent {
    constructor(renderer, el) {
        this.renderer = renderer;
        this.el = el;
    }
    ngOnInit() {
        let data = this.el.nativeElement.dataset;
        console.log(data);
        let script = this.renderer.createElement('script');
        if (data.src) {
            script.src = data.src;
        }
        if (data.clientid) {
            script.setAttribute('data-clientId', data.clientid);
        }
        if (data.token) {
            script.setAttribute('data-token', data.token);
        }
        if (data.apikey) {
            script.setAttribute('data-apiKey', data.apikey);
        }
        if (data.clientname) {
            script.setAttribute('data-clientName', data.clientnameS);
        }
        this.renderer.appendChild(this.el.nativeElement, script);
    }
}
CloudTablesComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesComponent, deps: [{ token: i0.Renderer2 }, { token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Component });
CloudTablesComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.0", type: CloudTablesComponent, selector: "cloud-tables", ngImport: i0, template: '', isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'cloud-tables',
                    template: '',
                    styles: []
                }]
        }], ctorParameters: function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3QtbGlicmFyeS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9jdC1saWJyYXJ5L3NyYy9saWIvY3QtbGlicmFyeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBaUMsTUFBTSxlQUFlLENBQUM7O0FBT3pFLE1BQU0sT0FBTyxvQkFBb0I7SUFFL0IsWUFBcUIsUUFBbUIsRUFBVSxFQUFjO1FBQTNDLGFBQVEsR0FBUixRQUFRLENBQVc7UUFBVSxPQUFFLEdBQUYsRUFBRSxDQUFZO0lBQUksQ0FBQztJQUVyRSxRQUFRO1FBQ04sSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDO1FBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkQsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFDO1lBQ1gsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFDO1lBQ2hCLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNyRDtRQUNELElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNkLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMvQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNqRDtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixNQUFNLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtTQUN6RDtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzNELENBQUM7O2lIQXhCVSxvQkFBb0I7cUdBQXBCLG9CQUFvQixvREFIckIsRUFBRTsyRkFHRCxvQkFBb0I7a0JBTGhDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLFFBQVEsRUFBRSxFQUFFO29CQUNaLE1BQU0sRUFBRSxFQUFFO2lCQUNYIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFJlbmRlcmVyMiwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjbG91ZC10YWJsZXMnLFxuICB0ZW1wbGF0ZTogJycsXG4gIHN0eWxlczogW11cbn0pXG5leHBvcnQgY2xhc3MgQ2xvdWRUYWJsZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGxldCBkYXRhID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LmRhdGFzZXQ7XG4gICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgbGV0IHNjcmlwdCA9IHRoaXMucmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgaWYgKGRhdGEuc3JjKXtcbiAgICAgIHNjcmlwdC5zcmMgPSBkYXRhLnNyYztcbiAgICB9XG4gICAgaWYgKGRhdGEuY2xpZW50aWQpe1xuICAgICAgc2NyaXB0LnNldEF0dHJpYnV0ZSgnZGF0YS1jbGllbnRJZCcsIGRhdGEuY2xpZW50aWQpO1xuICAgIH1cbiAgICBpZiAoZGF0YS50b2tlbikge1xuICAgICAgc2NyaXB0LnNldEF0dHJpYnV0ZSgnZGF0YS10b2tlbicsIGRhdGEudG9rZW4pO1xuICAgIH1cbiAgICBpZiAoZGF0YS5hcGlrZXkpIHtcbiAgICAgIHNjcmlwdC5zZXRBdHRyaWJ1dGUoJ2RhdGEtYXBpS2V5JywgZGF0YS5hcGlrZXkpO1xuICAgIH1cbiAgICBpZiAoZGF0YS5jbGllbnRuYW1lKSB7XG4gICAgICBzY3JpcHQuc2V0QXR0cmlidXRlKCdkYXRhLWNsaWVudE5hbWUnLCBkYXRhLmNsaWVudG5hbWVTKVxuICAgIH1cbiAgICB0aGlzLnJlbmRlcmVyLmFwcGVuZENoaWxkKHRoaXMuZWwubmF0aXZlRWxlbWVudCwgc2NyaXB0KTtcbiAgfVxufVxuIl19