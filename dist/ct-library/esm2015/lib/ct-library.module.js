import { NgModule } from '@angular/core';
import { CloudTablesComponent } from './ct-library.component';
import * as i0 from "@angular/core";
export class CloudTablesModule {
}
CloudTablesModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
CloudTablesModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, declarations: [CloudTablesComponent], exports: [CloudTablesComponent] });
CloudTablesModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        CloudTablesComponent
                    ],
                    imports: [],
                    exports: [
                        CloudTablesComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3QtbGlicmFyeS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9jdC1saWJyYXJ5L3NyYy9saWIvY3QtbGlicmFyeS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7QUFjOUQsTUFBTSxPQUFPLGlCQUFpQjs7OEdBQWpCLGlCQUFpQjsrR0FBakIsaUJBQWlCLGlCQVIxQixvQkFBb0IsYUFLcEIsb0JBQW9COytHQUdYLGlCQUFpQixZQU5uQixFQUNSOzJGQUtVLGlCQUFpQjtrQkFWN0IsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osb0JBQW9CO3FCQUNyQjtvQkFDRCxPQUFPLEVBQUUsRUFDUjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1Asb0JBQW9CO3FCQUNyQjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDbG91ZFRhYmxlc0NvbXBvbmVudCB9IGZyb20gJy4vY3QtbGlicmFyeS5jb21wb25lbnQnO1xuXG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQ2xvdWRUYWJsZXNDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbXG4gICAgQ2xvdWRUYWJsZXNDb21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBDbG91ZFRhYmxlc01vZHVsZSB7IH1cbiJdfQ==