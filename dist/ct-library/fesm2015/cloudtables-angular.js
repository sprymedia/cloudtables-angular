import * as i0 from '@angular/core';
import { Injectable, Component, NgModule } from '@angular/core';

class CloudTablesService {
    constructor() { }
}
CloudTablesService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
CloudTablesService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class CloudTablesComponent {
    constructor(renderer, el) {
        this.renderer = renderer;
        this.el = el;
    }
    ngOnInit() {
        let data = this.el.nativeElement.dataset;
        console.log(data);
        let script = this.renderer.createElement('script');
        if (data.src) {
            script.src = data.src;
        }
        if (data.clientid) {
            script.setAttribute('data-clientId', data.clientid);
        }
        if (data.token) {
            script.setAttribute('data-token', data.token);
        }
        if (data.apikey) {
            script.setAttribute('data-apiKey', data.apikey);
        }
        if (data.clientname) {
            script.setAttribute('data-clientName', data.clientnameS);
        }
        this.renderer.appendChild(this.el.nativeElement, script);
    }
}
CloudTablesComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesComponent, deps: [{ token: i0.Renderer2 }, { token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Component });
CloudTablesComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.0", type: CloudTablesComponent, selector: "cloud-tables", ngImport: i0, template: '', isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'cloud-tables',
                    template: '',
                    styles: []
                }]
        }], ctorParameters: function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }]; } });

class CloudTablesModule {
}
CloudTablesModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
CloudTablesModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, declarations: [CloudTablesComponent], exports: [CloudTablesComponent] });
CloudTablesModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.0", ngImport: i0, type: CloudTablesModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        CloudTablesComponent
                    ],
                    imports: [],
                    exports: [
                        CloudTablesComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of ct-library
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CloudTablesComponent, CloudTablesModule, CloudTablesService };
//# sourceMappingURL=cloudtables-angular.js.map
