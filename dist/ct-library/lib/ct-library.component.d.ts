import { OnInit, Renderer2, ElementRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class CloudTablesComponent implements OnInit {
    private renderer;
    private el;
    constructor(renderer: Renderer2, el: ElementRef);
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CloudTablesComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CloudTablesComponent, "cloud-tables", never, {}, {}, never, never>;
}
