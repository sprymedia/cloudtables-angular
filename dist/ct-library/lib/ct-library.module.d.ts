import * as i0 from "@angular/core";
import * as i1 from "./ct-library.component";
export declare class CloudTablesModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<CloudTablesModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<CloudTablesModule, [typeof i1.CloudTablesComponent], never, [typeof i1.CloudTablesComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<CloudTablesModule>;
}
