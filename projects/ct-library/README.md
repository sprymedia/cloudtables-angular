
# CloudTables Angular

CloudTables Angular is a component library that adds custom HTML tags `<cloud-tables></cloud-tables>` to your Angular application letting you embed a CloudTable anywhere in your Angular application.

[CloudTables](https://cloudtables.com/) is a no code / low code system which lets you create complex and dynamic database driven applications with ease. Hosted or self-hosted options are available so you can be up and running in moments.


## Getting Started

Inside the directory of your angular application created using:

```sh
ng new my_app
```

Then run:

```sh
cd my_app
npm install --save @cloudtables/angular
```

After npm has finished installing. Change your `app.module.ts` file in `/src/app` to import the CloudTables code:

```js
import { CloudTablesComponent, CloudTablesModule } from '@cloudtables/angular';
```

and in the `NgModule` section update the `imports` and `bootstrap` arrays to add the CloudTables module and component:

```js
imports: [
    // ...
    CloudTablesModule
],
bootstrap: [
    // ...
    CloudTablesComponent
]
```

Once those changes are made you can now use the custom CloudTables tags `<cloud-tables></cloud-tables>` to embed a CloudTable into your application - e.g.:

```html
<cloud-tables
    data-src="https://ct-examples.cloudtables.io/loader/4e9e8e3c-f448-11eb-8a3f-43eceac3195f/table/d"
    data-apiKey="AzG0e04UxhduaTAJjYC3Dgfr"
></cloud-tables>
```

* `data-src` The custom url for your CloudTable.
* `data-apiKey` would be replaced by your API Key (see the _Security / API Keys_ section in your CloudTables application)
* `data-token` server side generated secure access token that can be used instead of an `apiKey`
* `data-userId` is optional, but will be used to uniquely identify user's in the CloudTables interface.
* `data-userName` is also optional, but can be used to help identify who made what changes when reviewing logs in CloudTables. It is recommended you include `userId` and `userName`.

All the data values required can be found in your [CloudTables](https://cloudtables.com/) application.
