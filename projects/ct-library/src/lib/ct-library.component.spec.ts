import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudTablesComponent } from './ct-library.component';

describe('CloudTablesComponent', () => {
  let component: CloudTablesComponent;
  let fixture: ComponentFixture<CloudTablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CloudTablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
