import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';

@Component({
  selector: 'cloud-tables',
  template: '',
  styles: []
})
export class CloudTablesComponent implements OnInit {

  constructor( private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    let data = this.el.nativeElement.dataset;
    console.log(data);
    let script = this.renderer.createElement('script');
    if (data.src){
      script.src = data.src;
    }
    if (data.clientid){
      script.setAttribute('data-clientId', data.clientid);
    }
    if (data.token) {
      script.setAttribute('data-token', data.token);
    }
    if (data.apikey) {
      script.setAttribute('data-apiKey', data.apikey);
    }
    if (data.clientname) {
      script.setAttribute('data-clientName', data.clientnameS)
    }
    this.renderer.appendChild(this.el.nativeElement, script);
  }
}
