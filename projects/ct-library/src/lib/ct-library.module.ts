import { NgModule } from '@angular/core';
import { CloudTablesComponent } from './ct-library.component';



@NgModule({
  declarations: [
    CloudTablesComponent
  ],
  imports: [
  ],
  exports: [
    CloudTablesComponent
  ]
})
export class CloudTablesModule { }
