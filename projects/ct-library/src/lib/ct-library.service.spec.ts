import { TestBed } from '@angular/core/testing';

import { CloudTablesService } from './ct-library.service';

describe('CloudTablesService', () => {
  let service: CloudTablesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CloudTablesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
