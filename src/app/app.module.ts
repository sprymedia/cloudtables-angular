import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CTLibraryComponent, CTLibraryModule } from 'ct-library';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CTLibraryModule
  ],
  providers: [],
  bootstrap: [AppComponent, CTLibraryComponent]
})
export class AppModule { }
